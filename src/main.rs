mod capture;
mod cli;
mod widget;

use std::time::{Duration, Instant};

use anyhow::Result as AhResult;
use cava::builder::Channels;
use cava::{Builder, CavaInt};
use clap::Parser;
use dasp_sample::ToSample;
use ratatui::backend::CrosstermBackend;
use ratatui::crossterm::event;
use ratatui::crossterm::event::{
    DisableMouseCapture, EnableMouseCapture, Event, KeyCode, KeyEvent,
};
use ratatui::crossterm::execute;
use ratatui::crossterm::terminal::{
    disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen,
};
use ratatui::layout::Rect;
use ratatui::style::{Color, Style};
use ratatui::symbols;
use ratatui::symbols::bar::Set;
use ratatui::{DefaultTerminal, Frame, Terminal};
use ringbuffer::RingBuffer;

use capture::Capture;
use cli::{ChannelOrdering, Cli};
use widget::BarSpectrum;

const FPS: u64 = 60;
const FRAME_DURATION_MILIS: u64 = 1000 / FPS;
const MAX_BAR_V: u16 = 1000;

const STRIPE_BARS: Set = Set {
    full: "■",
    seven_eighths: "■",
    three_quarters: "■",
    five_eighths: "▬",
    half: "▬",
    three_eighths: "▬",
    one_quarter: " ",
    one_eighth: " ",
    empty: " ",
};

const BINARY_BARS: Set = Set {
    full: "■",
    seven_eighths: "■",
    three_quarters: "■",
    five_eighths: "■",
    half: "■",
    three_eighths: " ",
    one_quarter: " ",
    one_eighth: " ",
    empty: " ",
};

struct App {
    cava: CavaInt<u16>,
    channels: cava::builder::Channels,
    capture: Capture,
    output_buf: Vec<u16>,
    samples_per_frame: usize,
    bar_width: u16,
    bar_gap: u16,
    bar_set: Set,
    channel_ord: ChannelOrdering,
}

impl App {
    fn new(cli: Cli, screen: Rect) -> AhResult<Self> {
        let ch = Channels::Stereo;
        let sample_rate = 48_000;

        let &[bar_width, bar_gap] = cli.dims.as_slice() else {
            unreachable!("Bar and gap size should be enforced by clap");
        };
        let n_bars = cli.bars.unwrap_or_else(|| {
            widget::bar_capacity(screen, bar_width, bar_gap) as usize / ch.n_channels()
        });
        let noise_reduction = match cli.noise_filter {
            cli::NoiseFilter::Noisy => 0.15,
            cli::NoiseFilter::Normal => 0.46,
            cli::NoiseFilter::Smooth => 0.77,
        };

        let cava = Builder::new(n_bars, sample_rate)
            .channels(ch)
            .auto_sensitivity(true)
            .noise_reduction(noise_reduction)
            .lower_cut_off(50)
            .higher_cut_off(10_000)
            .integer()
            .max(MAX_BAR_V)
            .init()?;

        let capture = capture::start_capture("hw:Loopback,1")?;

        let bar_set = match cli.style {
            cli::BarStyle::Fill => symbols::bar::NINE_LEVELS,
            cli::BarStyle::Stripe => STRIPE_BARS,
            cli::BarStyle::Binary => BINARY_BARS,
        };

        let res = Self {
            cava,
            channels: ch,
            capture,
            output_buf: vec![0; n_bars * ch.n_channels()],
            samples_per_frame: (sample_rate / FPS as u32) as usize,
            bar_width,
            bar_gap,
            bar_set,
            channel_ord: cli.order,
        };

        Ok(res)
    }

    fn on_tick(&mut self) -> AhResult<()> {
        let sampled = self.capture.read_samples()?;
        let sampled: Vec<_> = sampled
            .drain()
            .take(self.samples_per_frame)
            .map(|v| v.to_sample_())
            .collect();

        self.cava.execute(&sampled, &mut self.output_buf)?;

        Ok(())
    }
}

fn main() -> AhResult<()> {
    let cli = Cli::parse();

    // setup terminal
    enable_raw_mode()?;
    let mut stdout = std::io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    let area = terminal.get_frame().area();

    let res = 'core: {
        let app = match App::new(cli, area) {
            Ok(app) => app,
            Err(err) => break 'core Err(err),
        };

        let tick_rate = Duration::from_millis(FRAME_DURATION_MILIS);
        run(&mut terminal, app, tick_rate)
    };

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    res
}

fn run(terminal: &mut DefaultTerminal, mut app: App, tick_rate: Duration) -> AhResult<()> {
    let mut last_tick = Instant::now();
    loop {
        terminal.draw(|f| render(f, &app))?;

        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));

        if event::poll(timeout)? {
            if let Event::Key(KeyEvent {
                code: KeyCode::Char('q') | KeyCode::Esc,
                ..
            }) = event::read()?
            {
                return Ok(());
            }
        }

        if last_tick.elapsed() >= tick_rate {
            app.on_tick()?;
            last_tick = Instant::now();
        }
    }
}

fn render(f: &mut Frame, app: &App) {
    let data: Vec<_> = match (app.channels, app.channel_ord) {
        (Channels::Mono, _) | (Channels::Stereo, ChannelOrdering::Split) => {
            app.output_buf.iter().map(|&v| v as u64).collect()
        }
        (Channels::Stereo, ChannelOrdering::Mirror) => {
            let half = app.output_buf.len() / 2;
            let left = &app.output_buf[..half];
            let right = &app.output_buf[half..];
            itertools::chain(left.iter().rev(), right)
                .map(|&v| v as u64)
                .collect()
        }
        (Channels::Stereo, ChannelOrdering::Group) => {
            let half = app.output_buf.len() / 2;
            let left = &app.output_buf[..half];
            let right = &app.output_buf[half..];
            itertools::interleave(left, right)
                .map(|&v| v as u64)
                .collect()
        }
    };

    let spectrum = BarSpectrum::default()
        .data(&data)
        .bar_width(app.bar_width)
        .bar_gap(app.bar_gap)
        .bar_set(app.bar_set.clone())
        .bar_base("═")
        .bar_style(Style::default().fg(Color::LightMagenta))
        .max(MAX_BAR_V as u64);

    f.render_widget(spectrum, f.area());
}
