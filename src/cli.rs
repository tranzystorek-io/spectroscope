use clap::{Parser, ValueEnum};

#[derive(ValueEnum, Clone, Copy, Debug)]
pub enum BarStyle {
    Fill,
    Stripe,
    Binary,
}

#[derive(ValueEnum, Clone, Copy, Debug)]
pub enum ChannelOrdering {
    Split,
    Mirror,
    Group,
}

#[derive(ValueEnum, Clone, Copy, Debug)]
pub enum NoiseFilter {
    Noisy,
    Normal,
    Smooth,
}

#[derive(Parser, Debug)]
pub struct Cli {
    /// Number of bars per channel to render
    ///
    /// By default, automatically picked to fill screen
    #[arg(short, long, value_name = "N")]
    pub bars: Option<usize>,

    /// Bar width and size of gaps between them
    #[arg(short, long, num_args = 2, value_names = ["WIDTH", "GAP"], default_values_t = [2, 1])]
    pub dims: Vec<u16>,

    /// Visual style of spectrum bars
    #[arg(short, long, value_enum, default_value_t = BarStyle::Stripe)]
    pub style: BarStyle,

    /// Display ordering of bars from separate audio channels
    #[arg(long, value_enum, default_value_t = ChannelOrdering::Mirror)]
    pub order: ChannelOrdering,

    /// Noise filtering level for the visualizer output
    #[arg(long, value_enum, visible_alias = "nf", value_name = "LEVEL", default_value_t = NoiseFilter::Noisy)]
    pub noise_filter: NoiseFilter,
}
