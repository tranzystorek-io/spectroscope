use alsa::pcm::*;
use alsa::{Direction, ValueOr};
use anyhow::Result as AhResult;
use ringbuffer::AllocRingBuffer;

const BUFFER_CAPACITY: usize = 8192;

pub struct Capture {
    pcm: PCM,
    audio_buf: AllocRingBuffer<i32>,
}

pub fn start_capture(device: &str) -> AhResult<Capture> {
    let pcm = PCM::new(device, Direction::Capture, false)?;
    {
        let hwp = HwParams::any(&pcm)?;
        hwp.set_channels(2)?;
        hwp.set_rate(48_000, ValueOr::Nearest)?;
        hwp.set_format(Format::s32())?;
        hwp.set_access(Access::MMapInterleaved)?;
        pcm.hw_params(&hwp)?;
    }
    pcm.start()?;
    Ok(Capture {
        pcm,
        audio_buf: AllocRingBuffer::new(BUFFER_CAPACITY),
    })
}

impl Capture {
    pub fn read_samples(&mut self) -> AhResult<&mut AllocRingBuffer<i32>> {
        let mut io = self.pcm.direct_mmap_capture()?;

        self.audio_buf.extend(io.iter().take(BUFFER_CAPACITY));

        Ok(&mut self.audio_buf)
    }
}
